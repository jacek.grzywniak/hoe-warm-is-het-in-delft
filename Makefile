IMAGE := hoe_warm_is_het_in_delft

lint:
	@echo "Running bandit against source files..."
	@bandit -r ./script
	@echo "Running black against source files..."
	@black ./script --check
	@echo "Running mypy against source files..."
	@mypy ./script
	@echo "Running flake8 against source files..."
	@flake8 ./script


build-prod:
	@echo "Building Production image: $(IMAGE)"
	@echo "Version: $(VERSION)"
	@docker build -t $(IMAGE):$(VERSION) .


run-prod:
	@echo "Running container from the production image: $(IMAGE)"
	@echo "Version: $(VERSION)"
	@docker run $(IMAGE):$(VERSION)
