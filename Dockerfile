FROM python:3.10.6-slim-bullseye AS build

ENV APP_HOME /hoe_warm_is_het_in_delft
WORKDIR $APP_HOME

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt .
RUN pip install -r requirements.txt --no-cache-dir && \
    adduser lime && \
    chown -R lime $APP_HOME

COPY script .


FROM build AS production

RUN chown lime:lime -R $APP_HOME
RUN chmod u+x $APP_HOME/HoeWarmIsHetInDelft.py
USER lime
ENTRYPOINT ["python", "HoeWarmIsHetInDelft.py"]
