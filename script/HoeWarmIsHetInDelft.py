import time

import requests
from requests import Response

DELFT_WEATHER_LINK = "https://weerindelft.nl/clientraw.txt"
CURRENT_TIME = time.strftime("%l:%M%p %Z on %b %d, %Y")
CURRENT_TIME_IN_MILLISECONDS = int(time.time() * 1000)


class ExternalSystemError(Exception):
    pass


def get_delft_weather_response() -> Response:
    return requests.get(f"{DELFT_WEATHER_LINK}?{CURRENT_TIME_IN_MILLISECONDS}")


def get_temperature_from_response(response: Response) -> float:
    try:
        temp_str: str = response.text.split()[4]
        return float(temp_str)
    except (IndexError, ValueError):
        raise ExternalSystemError(
            f"Could not retrieve temperature from {DELFT_WEATHER_LINK}."
        )


def show_temperature(temp: int) -> None:
    print(f"Current temperature in Delft: {temp} degrees Celsius")
    print(f"Temperature retrieved at: {CURRENT_TIME}")


def run() -> None:
    response: Response = get_delft_weather_response()
    temp: float = get_temperature_from_response(response)
    show_temperature(round(temp))


if __name__ == "__main__":
    run()
